import datetime
import pymysql
import container
import time
import traceback
import multiprocessing

def startlab(schedule):
    # Define the provided start time string
    #start_time_str = "2023-10-28T09:00:32.955Z"
    #print(">>>>",schedule)
    start_time_str = schedule[4]
    #print(start_time_str)

    # Correct the format by adding a colon between hours and minutes
    start_time_str = start_time_str.replace("T", " ").replace("Z", "")

    # Parse the corrected start time string into a datetime object
    start_time = datetime.datetime.strptime(start_time_str, "%Y-%m-%d %H:%M:%S.%f")

    # Get the current time
    #print(datetime.datetime.utcnow(), datetime.now())
    #time_difference = start_time - (datetime.datetime.utcnow()).astimezone(timezone('Asia/Kolkata'))

    print(schedule[0],datetime.datetime.now(),"----",start_time)
    time_difference = datetime.datetime.now() - start_time
    #print(time_difference);

    days = time_difference.days
    hours, remainder = divmod(time_difference.seconds, 3600)
    minutes, _ = divmod(remainder, 60)

    # Print the result
    print(f"Days: {days}, Hours: {hours}, Minutes: {minutes}")

    # Check if the time difference is 15 minutes or less
    #if time_difference <= datetime.timedelta(minutes=15) and time_difference >= datetime.timedelta():
    if days==-1 and hours==23 and minutes >= 50:
        # If within 15 minutes, trigger the function
        try:
            print("--> "+str(schedule))
            db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
            mycursor = db.cursor()
            print("DB connected")
            sql_qry= "UPDATE vlab.container_schedular SET status=%s WHERE id=%s " 
            values=("active",schedule[0])
            mycursor.execute(sql_qry,values)
            db.commit()
            print(mycursor.rowcount, "*******************was updated.******************")
            #container.startdocker(schedule)
            task_process = multiprocessing.Process(target=container.startdocker(schedule))
            task_process.start()
        except Exception as e:
            print("Exception: ",str(e))
            traceback.print_exc()
        #db.close()


        # id, creator_name, course_name, lab_type, start_date, stop_date, no_of_workbench, status, running_count, participants
        #('6', 'Mohammed Sahique', 'AIM', 'cloud computing', '2023-10-04T15:00:00.000Z', '2023-10-04T16:00:00.000Z', '10', 'inactive', NULL, NULL)
        # above is the example for parameter schedule
        
    else:
        print("docker not triggered at this time.")

def stoplab(schedule):
    # Define the provided start time string
    #start_time_str = "2023-10-28T09:00:32.955Z"
    #print(">>>>",schedule)
    stop_time_str = schedule[5]
    #print(stop_time_str)

    # Correct the format by adding a colon between hours and minutes
    stop_time_str = stop_time_str.replace("T", " ").replace("Z", "")

    # Parse the corrected stop time string into a datetime object
    stop_time = datetime.datetime.strptime(stop_time_str, "%Y-%m-%d %H:%M:%S.%f")

    # Get the current time
    print(schedule[0],datetime.datetime.now(),"----",stop_time)
    time_difference = stop_time - datetime.datetime.now()

    days = time_difference.days
    hours, remainder = divmod(time_difference.seconds, 3600)
    minutes, _ = divmod(remainder, 60)
    print(f"Days: {days}, Hours: {hours}, Minutes: {minutes}")
    # Check if the time difference is 15 minutes or less
    #if time_difference <= datetime.timedelta(minutes=15) and time_difference >= datetime.timedelta():
    if days==-1 and hours==23 and minutes <= 50:
        # If within 15 minutes, trigger the function
        try:
            print("--> "+str(schedule))
            db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
            mycursor = db.cursor()
            #print("DB connected")
        
            sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
            mycursor.execute(sql_qry,schedule[0])
            list =  mycursor.fetchall()
            #print(list); 
            for x in list: 
                #res=container.delete_container(x);    
                task_process = multiprocessing.Process(target=container.delete_container(x))
                task_process.start();   
            
            sql_qry= "UPDATE vlab.container_schedular SET status=%s WHERE id=%s " 
            values=("serviced",schedule[0])
            mycursor.execute(sql_qry,values)
            db.commit()
            print(mycursor.rowcount, "was updated.")
        except Exception as e:
            print("Exception: ",str(e))
            traceback.print_exc()
        #db.close()

    else:
        print("docker not deleted at this time.")

while True:

    db = pymysql.connect(
    host='db',
    user="root",
    password="root",
    db="vlab"
    )
    try:
        mycursor = db.cursor()
        print("DB connected")
        print("----------------------------------------------")
        sql_qry="SELECT * FROM vlab.container_schedular WHERE status='inactive' " 
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall()
        #print(list);
        for x in list:
            startlab(x)
        print("----------------------STOP------------------------")
        sql_qry="SELECT * FROM vlab.container_schedular WHERE status='active' " 
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall()
        
        for x in list:
            #print("list: ",+str(x))
            stoplab(x)
        
        db.close()
        
    except:
        print("DB not connected")
    time.sleep(60)
    

