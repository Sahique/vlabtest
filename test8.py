from flask import Flask, jsonify, request
from datetime import datetime, timedelta
import threading

app = Flask(__name__)

# Dictionary to store active user sessions and their last activity timestamp
active_sessions = {}

# Lock to synchronize access to active_sessions dictionary
lock = threading.Lock()

# Time threshold for considering a user inactive (e.g., 5 minutes)
inactive_threshold = timedelta(minutes=5)

def check_inactive_users():
    print("checking inactive users")
    
    global active_sessions
    print(active_sessions)
    with lock:
        now = datetime.utcnow()
        inactive_users = [user for user, last_activity in active_sessions.items() if (now - last_activity) > inactive_threshold]
        for user in inactive_users:
            print(f"Logging out inactive user: {user}")
            del active_sessions[user]
    # Reschedule the timer for the next iteration
    inactive_check_thread = threading.Timer(inactive_check_interval.total_seconds(), check_inactive_users)
    inactive_check_thread.start()
    
def heartbeat(user_id):
    with lock:
        active_sessions[user_id] = datetime.utcnow()
        print(f"Heartbeat received for user: {user_id}")

@app.route('/heartbeat', methods=['POST'])
def heartbeat_endpoint():
    user_id = request.json.get('user_id')
    if user_id:
        heartbeat(user_id)
        return jsonify({'status': 'success'})
    else:
        print("User ID not provided in the heartbeat request")
        return jsonify({'status': 'error', 'message': 'User ID not provided'}), 400

# Start a thread to check and log out inactive users periodically
inactive_check_interval = timedelta(minutes=1)
inactive_check_thread = threading.Timer(inactive_check_interval.total_seconds(), check_inactive_users)
inactive_check_thread.start()

if __name__ == '__main__':
    print("Flask app is running...")
    app.run(debug=True)
