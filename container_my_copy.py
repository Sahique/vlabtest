import docker
import pymysql
import socket
import mount
import traceback


start_port = 6940
end_port = 6999

free_ports = list(range(start_port, end_port + 1))
in_use_ports=[]
#print(free_ports)

def find_free_ports(num_port):
    allocated_ports=[]
    for x in range (0,num_port+1):
        port= free_ports.pop(0)
        allocated_ports.append(port)
    return allocated_ports

def dellocate_port(port):
    in_use_ports.remove(port)
    free_ports.append(port)
    print("port dellocated: ",port)


def startdocker(dockerData):
    response=[]
    # Initialize the Docker client
    print("starting docker ---- ",dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        
        #db = pymysql.connect(host='localhost', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        db = pymysql.connect(host='172.18.0.2', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        sql_qry= "SELECT * FROM vlab.labtype WHERE labName=%s " 
        mycursor.execute(sql_qry,dockerData[3])
        labTypes =  mycursor.fetchall()
        print(labTypes) ;  
        

        # Specify the image to use for the container
        image_name = labTypes[0]["imagePath"]
        sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
        print((sql_qry,dockerData[0]))
        mycursor.execute(sql_qry,dockerData[0])
        list =  mycursor.fetchall()
        print(list);  



        # Define the number of containers to create
        #num_containers = 0
        
        # Loop to create and start multiple containers
        #for i in range(num_containers):
        ##ports=[]
        ##startPort=6960 ; endPort=6999
        ##for _ in range(len(list)):
        ##    port = find_free_port()
        ##    print("Selected port:", port); ports.append(port)
        ##    startPort = port + 1 ;
        ##print(ports)
        ports = find_free_ports(len(list))
        i=0
        for x in list:
            #if (x["participantId"]!="none"):
            # Define the container directory path where the data will be available inside the container
            container_name = x["workbenchId"]
            host_port=6901
            container_port= ports[i]; i+=1;
            container_settings = {
                "image": image_name,
                "detach": True,
                "name": container_name,
                "ports": { f"{host_port}/tcp": container_port},
                "environment":{"VNC_PW": ""},
                "privileged": True,
                "volumes": [
                    "/home/Workspace",
                    "/home/Public",
                    # Add more volume paths as needed
                ]
                # You can specify more container settings here as needed
            }
            try:
                container = client.containers.create(**container_settings)
                print(f"Container {container_name} created with ID: {container.id}")

                # Start the container
                container.start()

                
                sql_qry= "UPDATE vlab.workbenchallocation SET link=%s WHERE workbenchId=%s "
                #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
            
                values=("http://139.14.11.48:"+str(container_port)+"/vnc.html",x["workbenchId"])
                mycursor.execute(sql_qry,values)
                db.commit()
                print(mycursor.rowcount, "was inserted.")
                print("docker created")


            except docker.errors.ImageNotFound:
                print(f"Image '{image_name}' not found.")
            except docker.errors.APIError as e:
                print(f"Docker API error: {e}")
        
    except Exception as e:
        #response["Ack"] = "exception: " + str(e)
        print("Exception: "+str(e))
        traceback.print_exc()
    
    db.close()
    return response

def delete_container(container_id):
    response=""
    #print("-->>",container_id,container_id["workbenchId"])
    try:
        client = docker.from_env()
        container = client.containers.get(container_id["workbenchId"])
        try:
            mount.umount_nfs_volume(container_id["workbenchId"], "139.14.11.59", "mnt/shared/user_"+container_id["participantId"], "/home/Workspace")
            mount.umount_nfs_volume(container_id["workbenchId"], "139.14.11.59", "mnt/shared/public", "/home/Public")
        except Exception as e:
            print("exception: "+str(e))

        container.remove(force=True)  # Force removal if the container is running
        print("Container " +container_id["workbenchId"]+" has been deleted.")
        response="Container " +container_id["workbenchId"]+" has been deleted."
        
        y=container_id['link'].split(":");  print(y)
        port=y[2].split("/"); print(port[0]);
        dellocate_port(port)
        
        try:
            db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
            mycursor = db.cursor()
            print("DB connected")
            sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s ,directorypath=%s, status=%s WHERE workbenchId=%s "
            #print(str(deallocation_data['bookingId']+str(x)),str(x),"none",deallocation_data['bookingId'])
            print("none","","serviced",container_id["workbenchId"])
            values=("none","","serviced",container_id["workbenchId"],)
            mycursor.execute(sql_qry,values)
            db.commit()
            print(mycursor.rowcount, "was inserted.")

        except Exception as e:
            print("exception: "+str(e))


    except docker.errors.NotFound as e:
        print(str(container_id["workbenchId"])+" not found: "+ str(e))
        response=str(container_id["workbenchId"])+" not found: "+ str(e)
    except docker.errors.APIError as e:
        print("An error occurred while deleting the container: "+str(e))
        response="An error occurred while deleting the container: "+str(e)
    return response

# Replace 'your_container_id' with the actual ID of the container you want to delete
#container_id = 'af9839f273c55e58535655aceb5aafd429624d5a67ec558e8c597019bc8504c9'

#delete_container(container_id)

#startdocker((39, '24', '1664', 'AIML', '2023-12-04T01:32:00.000Z', '2023-12-04T01:42:00.000Z', '2', 'active', '2023-12-04 01:23:00', '2023-12-04 01:00:00'))

#startdocker((31, '8', '2000', 'AIML', '2023-12-04T20:50:00.000Z', '2023-12-04T20:58:00.000Z', '2', 'active', '2023-12-04 01:23:00', '2023-12-04 01:00:00'))

def startdocker_new(dockerData):
    response=[]
    db = None  # Initialize db variable outside the try block
    # Initialize the Docker client
    print("starting docker ---- ",dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        
        db = pymysql.connect(host='172.18.0.2', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        ports=[]
        startPort=6930 ; endPort=6999
       
        #port = find_free_port(startPort, endPort)
        #print("Selected port:", port); ports.append(port)    
        print(ports)
        
        
        container_name = dockerData['course_id']+"_"+dockerData['id']
        host_port=6901
        container_port= ports; 
        container_settings = {
            "image": dockerData['image_name'],
            "detach": True,
            "name": container_name,
            "ports": { f"{host_port}/tcp": container_port},
            "environment":{"VNC_PW": ""},
            "privileged": True,
            "volumes": {"/home/urz/User" + dockerData['id']: {'bind': '/home/Workspace', 'mode': 'rw'}}

            # You can specify more container settings here as needed
        }
        try:
            container = client.containers.create(**container_settings)
            print(f"Container {container_name} created with ID: {container.id}")

            # Start the container
            container.start()
            url="http://139.14.11.48:"+str(container_port)+"/vnc.html"
            sql_qry= "INSERT INTO vlab.workbenchallocation (courseName, participantId,status,workbenchId,link) VALUES (%s,%s,%s,%s,%s) " 
            values=(dockerData['course_id'],dockerData['id'],"active",container_name,url,)
            mycursor.execute(sql_qry,values)
            #
            db.commit()
            print(str(mycursor.rowcount), "was inserted.")
            print(mycursor.rowcount, "was inserted.")
            if(mycursor.rowcount>0): response['Ack']= "workbench allocated"; response['link']=url

        except docker.errors.ImageNotFound:
            print(f"Image '{dockerData['image_name']}' not found.")
        except docker.errors.APIError as e:
            print(f"Docker API error: {e}")
    
    except Exception as e:
        response["Ack"] = "exception: " + str(e)
        print("Exception: "+str(e))
        traceback.print_exc()
    
    finally:
        # Close the database connection if it was successfully opened
        if db:
            db.close()
    return response['Ack']

#data= {"course_id":"12345","image_name":"sahiqaimlgermanylab","id":"40"}
#startdocker_new(data)







'''
import docker as docker_module

# Initialize the Docker client
client = docker_module.from_env()

# Define container configurations
containers = [
    {"name": "container1", "image": "your-docker-image:tag"},
    {"name": "container2", "image": "your-docker-image:tag"},
    # Add more container configurations as needed
]

# Create and start Docker containers
for container_config in containers:
    container_name = container_config["name"]
    container_image = container_config["image"]
    
    # Check if the container already exists, and remove it if it does
    try:
        existing_container = client.containers.get(container_name)
        existing_container.remove(force=True)
    except docker_module.errors.NotFound:
        pass
    
    # Create and start the container
    container = client.containers.run(
        image=container_image,
        name=container_name,
        detach=True,  # Run the container in the background
        # Add more container options as needed
    )
    
    print(f"Container {container_name} started with ID: {container.id}")
'''
