# Use the official Python image as the base image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

RUN apt-get update && \
    apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list && \
    apt-get update && \
    apt-get install -y docker-ce docker-ce-cli containerd.io

# Setting up time zone for docker container
RUN apt-get update && apt-get install -y tzdata
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# Install Flask and other dependencies
RUN pip install -r requirements.txt
RUN pip install cryptography

# Copy the SSL certificates into the container
COPY cert/fullchain.pem /etc/ssl/private/fullchain.pem
COPY cert/privkey.pem /etc/ssl/private/privkey.pem


# Ensure the permissions are correct inside the container
RUN chmod 644 /etc/ssl/private/fullchain.pem
RUN chmod 600 /etc/ssl/private/privkey.pem


# Expose port 5000
EXPOSE 5000

# Define the command to run your application
#CMD ["python", "app.py","scheduler.py"]
CMD ["/app/entrypoint.sh"]