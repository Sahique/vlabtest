import socket

def find_free_port(start_port, end_port):
    for port in range(start_port, end_port + 1):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)  # Adjust the timeout based on your needs

        result = sock.connect_ex(('139.14.11.48', port))
        #result = sock.connect_ex(('127.0.0.1', port))
        if result != 0:
            # Port is available
            print("port no>> ",str(port))
            return port

    raise Exception("No available ports ")

ports=[]
startPort=6930 ; endPort=6999
for _ in range(len(list)):
    port = find_free_port(startPort, endPort)
    print("Selected port:", port); ports.append(port)
    startPort = port + 1 ;
print(ports)