
from flask import Flask, render_template, request,jsonify
import pymysql
import json
import math
from datetime import datetime, timedelta
import mount
import os
import traceback
import container
import threading

app = Flask(__name__)
# https://gitlab.com/Sahique/vlabtest.git


# MySQL configuration
hostname="db"    # this will be "db" for docker and "localhost" for local testing
db = pymysql.connect(
    host=hostname,  # This should be the service name defined in Docker Compose
    user='root',
    password='root',
    db='vlab',  # Replace with your database name
    cursorclass=pymysql.cursors.DictCursor
)
try:
    mycursor = db.cursor()
    print("DB connected")
    mycursor.close()
except:
    print("DB not connected")


def create_port_table(start_port, end_port):
    db = pymysql.connect(
        host=hostname,  # This should be the service name defined in Docker Compose
        user='root',
        password='root',
        db='vlab',  # Replace with your database name
        cursorclass=pymysql.cursors.DictCursor
    )
    try:
        mycursor = db.cursor()
        print("DB connected")
        mycursor.close()
    except:
        print("DB not connected")

    # Create tables
    with db.cursor() as cursor:
        # Create table for allocated ports
        cursor.execute("DROP TABLE IF EXISTS allocated_ports")
        cursor.execute("DROP TABLE IF EXISTS free_ports")
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS allocated_ports (
            id INT AUTO_INCREMENT PRIMARY KEY,
            port INT
        )
        """)

        # Create table for free ports
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS free_ports (
            id INT AUTO_INCREMENT PRIMARY KEY,
            port INT
        )
        """)

    # Commit changes and close connection
    db.commit()



    try:
        with db.cursor() as cursor:
            # Insert port numbers into the free_ports table
            for port in range(start_port, end_port):  # 7000 is exclusive
                sql = "INSERT INTO free_ports (port) VALUES (%s)"
                cursor.execute(sql, (port,))
        
        # Commit changes
        db.commit()
        print("Port numbers inserted successfully.")

    finally:
        # Close connection
        db.close()

@app.route('/test')
def test():
    return("Hello, the server is running")

@app.route('/hello')
def hello():
    mycursor = db.cursor()
    mycursor.execute('SELECT * FROM vlab.container_schedular')  # Replace with your table name
    data = mycursor.fetchall()
    mycursor.close()
    return (jsonify(data))

@app.route('/admin')
def admin():
    return render_template("index_admin.html")

@app.route('/teacher')
def teacher():
    
    return render_template("index_teacher.html")

@app.route('/student')
def student():
    return render_template("index_student.html")

@app.route('/')
def loginpage():
    return render_template("login.html")

@app.route('/fileUpload')
def fileUpload():
    return render_template('upload.html')

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return "No file part"

    file = request.files['file']

    if file.filename == '':
        return "No selected file"

    try:
        # Save the file to the NFS server directory
        nfs_server_path="139.14.11.59:/mnt/shared/"
        file.save(os.path.join(nfs_server_path, file.filename))
        return "File uploaded successfully"
    except Exception as e:
        return f"Error uploading file: {str(e)}"


@app.route('/vlab')
def vlab():
    details="admin,sahique"
    return render_template("vlab.html",val=details)

def checkslots(bookingData):
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        mycursor = db.cursor()
        sql_qry="SELECT * FROM vlab.container_schedular WHERE lab_type=%s AND status=%s"
        values=(bookingData['Lab Type'],"inactive",)
        mycursor.execute(sql_qry,values)
        list =  mycursor.fetchall();    
        print(list);   
        ack=""
        if (len(list)<=0):
            ack="success"
        else:
            for x in list:  
                print(">>>>>>>>>>>>",x)          
                # Define your time slots in the given format
                slot1_start = datetime.strptime(x['start_date'], '%Y-%m-%dT%H:%M:%S.%fZ')
                slot1_end = datetime.strptime(x['stop_date'], '%Y-%m-%dT%H:%M:%S.%fZ')

                #slot2_start = datetime.strptime('2023-09-28T11:00:00.000Z', '%Y-%m-%dT%H:%M:%S.%fZ')
                #slot2_end = datetime.strptime('2023-09-28T12:00:00.000Z', '%Y-%m-%dT%H:%M:%S.%fZ')

                # Input date and time to check
                check_time = datetime.strptime(bookingData['Start Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')

                # Check if the check_time falls within any slot
                found_slot = None

                if (slot1_start <= check_time <= slot1_end and bookingData['Creator id'] == x['id']):
                    ack="failed"
                elif not (slot1_start <= check_time <= slot1_end and bookingData['Creator id'] == x['id']):
                    ack="success"
                elif  (slot1_start <= check_time <= slot1_end and bookingData['Creator id'] != x['id']):
                    ack="success"
                if (ack=="success"): break            
    except:
        ack="DB not connected"
        print("DB not connected")
        
    #db.close()
    return ack


@app.route('/olatLogin', methods=['POST'])
def olatLogin():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure","template_name":"","details":""}
        # get the bookig id so that with that id labs can be used
        #json_data = {"id":"12345","role":"admin", "course name":"Embedded systems", "booking_id":"" }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print("inside ++>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        login_data = json.loads(data)
        
        template_name=""; details=""
        response['Ack']="success"
        response['template_name']="http://139.14.11.65:6940/vnc.html"; response['details']=login_data['role']+","+login_data['id']+","+login_data['course name']
        #if(login_data['role']=="Admin"):    template_name="index.html"; details=login_data['role']+","+login_data['id']
        #elif(login_data['role']=="Teacher"):    template_name= "index_teacher.html"; details=login_data['role']+","+login_data['id']
        #elif(login_data['role']=="Learner"):    template_name="index_student.html" ; details=login_data['role']+","+login_data['id']
        #return render_template(template_name,val=details)
        #return render_template("index_teacher.html",val=login_data['role']+","+login_data['id']+","+login_data['course name'])
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
    db.close()
    print(response)
    return response

@app.route('/login', methods=['POST'])
def login():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure","template_name":"","details":""}
        # get the bookig id so that with that id labs can be used
        #json_data = {"id":"12345","role":"admin", "course name":"Embedded systems", "bookingId":"" }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print("inside ++>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        login_data = json.loads(data)

        sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE participantId=%s AND bookingId=%s "
        values=(login_data['id'],login_data['bookingId'],)
        mycursor.execute(sql_qry,values)
        duplicate_data =  mycursor.fetchall(); 
        if(len(duplicate_data)>0):
            response['Ack']="user alredy allocated with workbench." ;    response['link']=duplicate_data[0]["link"]
        else:
            sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE participantId=%s AND bookingId=%s "
            values=("none",login_data['bookingId'],)
            mycursor.execute(sql_qry,values)
            allocation_data =  mycursor.fetchall(); 
            print(allocation_data)
            if(len(allocation_data)>0):
                mounting_status_public=mount.mount_nfs_volume("",allocation_data[0]["workbenchId"], "139.14.11.59", "mnt/shared/public", "/home/Public")
                print("mounting public :"+str(mounting_status_public))  #mounting_status="success"
            
                mounting_status_user=mount.mount_nfs_volume(login_data['id'],allocation_data[0]["workbenchId"], "139.14.11.59", "mnt/shared/user_"+login_data['id'], "/home/Workspace")
                print("mounting status: "+ str(mounting_status_user))
                
                if(mounting_status_user=="success" ): #and mounting_status_public=="success"):
                    #sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s, directorypath=%s, status=%s, session_time=%s WHERE workbenchId=%s "
                    sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s, directorypath=%s, status=%s WHERE workbenchId=%s "
                    #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
                    #print(login_data['id'],"mnt/shared/user_"+login_data['id'],datetime.utcnow(),"active")
                    #values=(login_data['id'],"mnt/shared/user_"+login_data['id'],"active",datetime.utcnow(),allocation_data[0]["workbenchId"],)
                    print(login_data['id'],"mnt/shared/user_"+login_data['id'],"active")
                    values=(login_data['id'],"mnt/shared/user_"+login_data['id'],"active",allocation_data[0]["workbenchId"],)
                    mycursor.execute(sql_qry,values)
                    db.commit()
                    print(mycursor.rowcount, "was inserted.")
                    if(mycursor.rowcount>0): response['Ack']= "workbenches allocated"; response['link']=allocation_data[0]["link"]
                else: response['Ack']="user mount "+str(mounting_status_user)   #+", public folder mount: "+str(mounting_status_public);
            else:   response['Ack']="All workbenches are occupied."
    except Exception as e:
        response['Ack']="Exception Occured: ",str(e)
        traceback.print_exc()
    db.close()
    print(response)
    return response

# Lock to synchronize access to active_sessions dictionary
lock = threading.Lock()

# Time threshold for considering a user inactive (e.g., 5 minutes)
inactive_threshold = timedelta(minutes=5)

def check_inactive_users():
    print("checking inactive users")
    db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
    mycursor = db.cursor()
    print("DB connected")
    with lock:
        sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE status=%s"
        values=("active",)
        mycursor.execute(sql_qry,values)
        session_data =  mycursor.fetchall(); 
        now = datetime.utcnow()
        for x in session_data[0]:
            print(now - x['session_time'])
            if ( now - x['session_time']) > inactive_threshold:
                print(f"Logging out inactive user: {x['participantId']}")
                sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s, directorypath=%s, status=%s, session_time=%s WHERE workbenchId=%s "
                #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
                values=("none","","inactive","",x['workbenchId'])
                mycursor.execute(sql_qry,values)
                db.commit()
                print(mycursor.rowcount, "was inserted.")

    # Reschedule the timer for the next iteration
    #inactive_check_thread = threading.Timer(inactive_check_interval.total_seconds(), check_inactive_users)
    #inactive_check_thread.start()

def heartbeat(user_id,booking_id):
    with lock:
        sql_qry= "UPDATE vlab.workbenchallocation SET session_time=%s WHERE participantId=%s AND bookingId=%s "
        #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
        values=(datetime.utcnow(),user_id,booking_id,)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was inserted.")
        print(f"Heartbeat received for user: {user_id}")

@app.route('/heartbeat', methods=['POST'])
def heartbeat_endpoint():
    user_id = request.json.get('id')
    bookingId = request.json.get('bookingId')
    if user_id:
        heartbeat(user_id,bookingId)
        return jsonify({'status': 'success'})
    else:
        print("User ID not provided in the heartbeat request")
        return jsonify({'status': 'error', 'message': 'User ID not provided'}), 400

# Start a thread to check and log out inactive users periodically
#inactive_check_interval = timedelta(minutes=1)
#inactive_check_thread = threading.Timer(inactive_check_interval.total_seconds(), check_inactive_users)
#inactive_check_thread.start()

@app.route('/login_new', methods=['POST'])
def login_new():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure","template_name":"","details":""}
        # get the bookig id so that with that id labs can be used
        #json_data = {"id":"12345","role":"admin", "course name":"Embedded systems", "booking_id":"" }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print("inside ++>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        login_data = json.loads(data)

        sql_qry= "SELECT * FROM vlab.labtype  WHERE labName=%s" 
        values=(login_data['course name'],);    mycursor.execute(sql_qry,values)
        labTypes =  mycursor.fetchall()
        print(labTypes)
        if(len(labTypes)>0):
            ack=container.startdocker_new({"course_id":login_data['course name'],"image_name":labTypes[0]['imagePath'],"id":login_data['id']})
            response['Ack']=ack;
        
    except Exception as e:
        response['Ack']="exception: ",str(e)
        traceback.print_exc()
    db.close()
    print(response)
    return response


@app.route('/logout', methods=['POST'])
def logout():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure","template_name":"","details":""}
        # get the bookig id so that with that id labs can be used
        #json_data = {"id":"12345","role":"admin", "course name":"Embedded systems", "booking_id":"" }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print("inside ++>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        login_data = json.loads(data)

        sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE participantId=%s AND bookingId=%s "
        values=(login_data['id'],login_data['bookingId'],)
        mycursor.execute(sql_qry,values)
        deallocation_data =  mycursor.fetchall(); 
        print(deallocation_data)
        if(len(deallocation_data)>0):
            demounting_status_user=mount.umount_nfs_volume(deallocation_data[0]["workbenchId"], "139.14.11.59", "mnt/shared/user_"+login_data['id'], "/home/Workspace")
            demounting_status_public=mount.umount_nfs_volume(deallocation_data[0]["workbenchId"], "139.14.11.59", "mnt/shared/public", "/home/Public")
            #mounting_status="success"
            if(demounting_status_user=="success" ): #and demounting_status_public=="success"):
                sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s ,directorypath=%s, status=%s WHERE workbenchId=%s "
                #print(str(deallocation_data['bookingId']+str(x)),str(x),"none",deallocation_data['bookingId'])
                print("none","","inactive",deallocation_data[0]["workbenchId"])
                values=("none","","inactive",deallocation_data[0]["workbenchId"],)
                mycursor.execute(sql_qry,values)
                db.commit()
                print(mycursor.rowcount, "was inserted.")
                if(mycursor.rowcount>0): response['Ack']= "workbenches deallocated";
            else: response['Ack']="user unmount "+str(demounting_status_user)  #+", public folder unmount: "+str(demounting_status_public);
        else:   response['Ack']="workbench not found."
    except Exception as e:
        response['Ack']="exception: ",str(e)
        traceback.print_exc()
    db.close()
    print(response)
    return response



@app.route('/loginVlab', methods=['POST'])
def loginvlab():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure","template_name":"","details":""}
        # get the bookig id so that with that id labs can be used
        #json_data = {"id":"1","name":"sahique","email":"msahique@gmail.com","role":"admin",}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print("inside ++>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        login_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry="SELECT * FROM vlab.registration WHERE email=%s AND password=%s " 
        values=(login_data['email'],login_data['password'])
        mycursor.execute(sql_qry,values)
        list =  mycursor.fetchall()
        print(list,mycursor.rowcount);  mycursor.close()
        if(mycursor.rowcount==1): 
            response['Ack']="User found"; response['template_name']=list[0]['role']; response['details']=list[0]

        else:     response['Ack']="User not found. Enter valid credentails "
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

################## Booking APIS ##########################
import secrets
import string

def generate_random_token(length=32):
    # Define the characters that can be used in the token (letters and digits)
    characters = string.ascii_letters + string.digits
    # Generate a random token with the specified length
    token = ''.join(secrets.choice(characters) for _ in range(length))
    return token


@app.route('/createBooking', methods=['POST'])
def createBooking():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        print("1")
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        booking_data = json.loads(data)
        #booking_data2={
        #  "Creator id": "John mathew",   "Course Name": "Embedded Systems",  "Lab Type": "Micro Controller lab",
        #  "Number of workbench": 15,   "Start Date-Time": "2023-09-28T17:49:32.955Z","End Date-Time": "2023-10-28T17:49:32.955Z"
        #}
        print("2")
        mycursor = db.cursor()
        l_sql_qry="SELECT * FROM vlab.container_schedular " 
        mycursor.execute(l_sql_qry)
        list =  mycursor.fetchall()
        print("---------------------------------------------")
        print(list);    
        duplicate=False
        print("3")
        for x in list:
            if (x['creator_id'] == booking_data['Creator id'] and x['course_name']== booking_data['Course Name'] and x['start_date']== booking_data['Start Date-Time']):
                duplicate=True
                print("4")
        if(duplicate==False):
            storage_check=checkStroageForImage(2,4,booking_data['Number of workbench'])
            checkforslot=checkslots(booking_data)
            print("5")
            print("strorage_check::::",storage_check,checkforslot)
            input_datetime = datetime.strptime(booking_data['Start Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')
            print("inputdatime>>>",input_datetime)
            print("6")
            if( input_datetime > datetime.now() ):
                print("7")
                # Add 15 minutes
                #print("new_datetime_after_addition>>>",new_datetime_after_addition)
                #print(type(input_datetime),type(timedelta(minutes=15)))
                new_datetime_after_addition = (input_datetime) + timedelta(minutes=15)
                #print("new_datetime_after_addition>>>>>>>>>",new_datetime_after_addition)
                

                # Subtract 15 minutes
                input_stop_datetime = datetime.strptime(booking_data['End Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')
                new_datetime_after_subtraction = (input_stop_datetime) - timedelta(minutes=15)
                print("8")

                if(storage_check['Ack']=="success" and checkforslot=="success"):
                    print("9")
                    token=generate_random_token()
                    sql_qry= "INSERT INTO vlab.container_schedular (creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status,buffer_start_datime,buffer_stop_datime) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s) " 
                    values=(booking_data['Creator id'],booking_data['Course Name'],booking_data['Lab Type'],booking_data['Start Date-Time'],booking_data['End Date-Time'],booking_data['Number of workbench'],"inactive", str(new_datetime_after_addition),str(new_datetime_after_subtraction))
                    mycursor.execute(sql_qry,values)
                    db.commit()
                    print(mycursor.rowcount, "was inserted.")
                    booking_id = mycursor.lastrowid
                    
                    if(mycursor.rowcount>0):    
                        
                        response['Ack']="Slots booked successfully with id : "+ str(booking_id)+", Token: "+str(token)
                        for x in range(0,int(booking_data['Number of workbench'])):
                            sql_qry= "INSERT INTO vlab.workbenchallocation (courseName,bookingId, participantId,status,workbenchId) VALUES (%s,%s,%s,%s,%s) " 
                            values=(booking_data['Course Name'],booking_id,"none","inactive",str(booking_data['Course Name'])+"_"+str(booking_id)+"_"+str(x))
                            mycursor.execute(sql_qry,values)
                        #
                        db.commit()
                        print(str(mycursor.rowcount), "was inserted.")
                        
                        #if(mycursor.rowcount>0):    response['Ack']="workbench created"
                else:
                    if(storage_check['Ack']=="fail" ):   response['Ack']="resources not available"
                    if( checkforslot=="fail"):    response['Ack']="slots over lapping"
            else:
                response['Ack']="wrong date and time."
        else:
            response['Ack']="Already booked for the parameters given."
    except Exception as e:
        # Handle the exception and print the error message
        print("An exception occurred: " ,str(e))
        response['Ack']="exception error: "+str(e) 
        #print("DB not connected")
    db.close()
    return response

@app.route('/editBooking', methods=['POST'])
def editBooking():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        

        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        edit_data = json.loads(data)
    
        #{
        #  "id":"2", "Creator id": "Ana mendis",   "Course Name": "Embedded Systems",  "Lab Type": "HDL lab",
        #  "Number of workbench": 20,    "Start Date-Time": "2023-09-28T10:00:32.955Z",  "End Date-Time": "2023-10-28T11:00:32.955Z"
        #}
        mycursor = db.cursor()
        sql_qry= "UPDATE vlab.container_schedular SET creator_id=%s, course_name=%s, lab_type=%s, start_date=%s, stop_date=%s, no_of_workbench=%s, status=%s WHERE id=%s " 
        values=(edit_data['Creator id'],edit_data['Course Name'],edit_data['Lab Type'],edit_data['Start Date-Time'],edit_data['End Date-Time'],edit_data['Number of workbench'],edit_data['status'],edit_data['id'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was updated."); mycursor.close();
        if(mycursor.rowcount>0):    response['Ack']="Booking details updated successfully"
    except Exception as e:
        # Handle the exception and print the error message
        print("An exception occurred: " ,str(e))
        response['Ack']="exception error: "+str(e) 
        
    db.close()
    return response

@app.route('/listBooking', methods=['POST'])
def listBooking():
    # eg:   {"Lab Type": "HDL lab","Creator id": "Ana mendis"}
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": ""}
        booking_data={
        "Creator id": "John mathew",
        "Course Name": "Embedded Systems",
        "Lab Type": "Micro Controller lab",
        "Number of workbench": 15,
        "Start Date-Time": "2023-09-22T17:49:32.955Z",
        "End Date-Time": "2023-09-22T17:49:32.955Z"
        }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        list_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry=""; base_qry="SELECT * FROM vlab.container_schedular  " 
        filter_conditions = []
        try:
            if list_data['Creator id']:   filter_conditions.append(f"creator_id = '{list_data['Creator id']}'")
        except: pass
        try:
            if list_data['Course Name']:   filter_conditions.append(f"course_name = '{list_data['Course Name']}'")
        except: pass
        try:    
            if list_data['Lab Type']:   filter_conditions.append(f"lab_type = '{list_data['Lab Type']}'")
        except: pass
        try:
            if list_data['Number of workbench']:   filter_conditions.append(f"no_of_workbench = '{list_data['Number of workbench']}'")
        except: pass
        try:
            if list_data['Start Date-Time'] and list_data['End Date-Time']:   filter_conditions.append(f"start_date BETWEEN '{list_data['Start Date-Time']}' AND '{list_data['End Date-Time']}'")
            else: 
                if list_data['Start Date-Time'] :   filter_conditions.append(f"start_date = '{list_data['Start Date-Time']}'")
                if list_data['End Date-Time'] :   filter_conditions.append(f"stop_date = '{list_data['End Date-Time']}'")
        except: pass

        if filter_conditions:
            filter_sql = " AND ".join(filter_conditions)
            sql_qry = f"{base_qry} WHERE {filter_sql}"
        else:
            #if (list_data['role']=="admin"):
            sql_qry = base_qry
            #else:
            #   response['Ack'] = "No parameter given"

        #values=(list_data['Creator id'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
        print(sql_qry)
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall();    
        print(list);    
        if (len(list)>0): response['Ack'] = list
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/deleteBooking', methods=['POST'])
def deleteBooking():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": ""}
        # {"id":5}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        delete_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry= " DELETE FROM vlab.container_schedular WHERE id=%s " 
        values=(delete_data['id'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was deleted.");   mycursor.close()
        if(mycursor.rowcount>0): 
            db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
            mycursor = db.cursor()
            print("DB connected")
            sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s " 
            values=(delete_data['id'],)
            mycursor.execute(sql_qry,values)
            list =  mycursor.fetchall();    mycursor.close();
            print(list);    
            if (len(list)>0):
                try:
                    for x in list:
                        container.delete_container(x);
                except ValueError as e:
                    print(f"Error: {str(e)}")
            response['Ack']="booking was deleted "
        else: response['Ack']="booking Id not found, please check the booking Id. "
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response


################## Workbench APIS ##########################

# NOTE: creation of workbenches occurs while booking a lab.
  
@app.route('/listWorkbench', methods=['POST'])
def listWorkbench():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        # eg:   {"Lab Type": "HDL lab","Creator id": "Ana mendis"}
        response={"Ack": ""}
        listAllocation_data={
        "workbenchId": "",
        "bookingId": 5,
        "participantId": ""
        }
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        list_workbench = json.loads(data)
        mycursor = db.cursor()
        sql_qry=""; base_qry="SELECT * FROM vlab.workbenchallocation  " 
        filter_conditions = []
        try:
            if list_workbench['workbenchId']:   filter_conditions.append(f"workbenchId = '{list_workbench['workbenchId']}'")
        except: pass
        try:
            if list_workbench['courseName']:   filter_conditions.append(f"courseName = '{list_workbench['courseName']}'")
        except: pass
        try:
            if list_workbench['status']:   filter_conditions.append(f"status = '{list_workbench['status']}'")
        except: pass
        try:
            if list_workbench['link']:   filter_conditions.append(f"link = '{list_workbench['link']}'")
        except: pass
        try:
            if list_workbench['bookingId']:   filter_conditions.append(f"bookingId = '{list_workbench['bookingId']}'")
        except: pass
        try:    
            if list_workbench['participantId']:   filter_conditions.append(f"participantId = '{list_workbench['participantId']}'")
        except: pass

        if filter_conditions:
            filter_sql = " AND ".join(filter_conditions)
            sql_qry = f"{base_qry} WHERE {filter_sql}"
            print(sql_qry)
        else:
            if (list_workbench['role']=="admin"):
                sql_qry = base_qry
            else:
                response['Ack'] = "No parameter given"

        #values=(list_data['Creator id'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall();    mycursor.close();
        print(list);    
        if (len(list)>0): response['Ack'] = list
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/allocateWorkbench', methods=['POST'])
def allocateWorkbench():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        allocation_data = json.loads(data)
        #{"bookingId":"5", "participants":[45,12]]}
        mycursor = db.cursor()
        sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE participantId=%s AND bookingId=%s "
        values=("none",allocation_data['bookingId'],)
        mycursor.execute(sql_qry,values)
        booking_data =  mycursor.fetchall(); 
        if(len(booking_data[0])>len(allocation_data['participants'])):
            i=0;
            for x in allocation_data['participants']:
                sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s AND directorypath=%s WHERE workbenchId=%s AND bookingId=%s "
                #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
                print(str(x),booking_data[i][1],allocation_data['bookingId'])
                values=(str(x),booking_data[i][1],allocation_data['bookingId'],"C:/Users/"+str(x)+"/workspace/",)
                mycursor.execute(sql_qry,values)
                db.commit()
                print(mycursor.rowcount, "was inserted.")
                i=i+1;

                if(mycursor.rowcount>0): response['Ack']=str(i)+" workbenches allocated. "
            mycursor.close()
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/deallocateWorkbench', methods=['POST'])
def deallocateWorkbench():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack":""}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        deallocation_data = json.loads(data)
        #{"workbenchIdandparticipantId":[["12_0","45"],["12_1","85"]]}
        mycursor = db.cursor() 
        for x in deallocation_data['workbenchIdandparticipantId']:
            sql_qry= "UPDATE vlab.workbenchallocation SET  participantId=%s WHERE workbenchId=%s AND participantId=%s "
            values=("none",x[0],x[1])
            mycursor.execute(sql_qry,values)
            db.commit()
            print(mycursor.rowcount, "was updated."); mycursor.close()
            if(mycursor.rowcount>0): 
                response['Ack']=response['Ack']+" workbench deallocated for participant with id "+x[1]+"  ,"
            print(response)   
            
        if(response['Ack']==""):    
            response['Ack']=="failed"
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/editWorkbench', methods=['POST'])
def editWorkbench():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        workbench_data = json.loads(data)
    
        #{
        #  "id":"2", "Creator id": "Ana mendis",   "Course Name": "Embedded Systems",  "Lab Type": "HDL lab",
        #  "Number of workbench": 20,    "Start Date-Time": "2023-09-28T10:00:32.955Z",  "End Date-Time": "2023-10-28T11:00:32.955Z"
        #}
        mycursor = db.cursor()
        sql_qry= "UPDATE vlab.workbenchallocation SET workbenchId=%s, bookingId=%s, participantId=%s, directorypath=%s, courseName=%s, status=%s, link=%s  WHERE sno=%s " 
        
        values=(workbench_data['workbenchId'],workbench_data['bookingId'],workbench_data['participantId'],workbench_data['directorypath'],workbench_data['courseName'],workbench_data['status'],workbench_data['link'],workbench_data['sno'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was updated."); mycursor.close()
        if(mycursor.rowcount>0):    response['Ack']="Booking details updated successfully"
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

################## Lab type APIS ##########################
@app.route('/listLabType', methods=['GET'])
def listLabType():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        #data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        #print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        #list_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry= "SELECT * FROM vlab.labtype  " 
        mycursor.execute(sql_qry)
        labTypes =  mycursor.fetchall()
        print(labTypes) ;   mycursor.close()
        response['Ack']= labTypes
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

################## Infrastructure APIS #####################
@app.route('/updateInfra', methods=['POST'])
def updatecorestroage():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        #response={"redirect": ""}
        #try:
        mycursor = db.cursor()
        sql_qry= "SELECT * FROM vlab.resources ORDER BY Sno DESC LIMIT 1  " 
        mycursor.execute(sql_qry)
        response =  mycursor.fetchall();    print(response)
        # {   "coreaction":"add", "corecount":5, "storageaction":"remove", "storagecount":1  }
        #   {"cores":"",    "ram":"",   "storage":""}
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        update_data = json.loads(data)
        sql_qry= "INSERT INTO vlab.container_schedular (coreallocated, corecredit, coretoal, storageallocated, storagecredit, stroragetotal, cpu,ram, totalcores, totalstroage) VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%d,%d) " 
        if ((response[0][1]+response[0][3])==response[0][9] and (response[0][4]+response[0][6])==response[0][10]):
            if(update_data['coreaction']=="add"):
                response[0][8]=response[0][8]+update_data['corecount']
            else:
                response[0][8]=response[0][8]-update_data['corecount']    
            
            if(update_data['storageaction']=="add"):
                response[0][9]=response[0][9]+ (update_data['storagecount']*1000)
            else:
                response[0][9]=response[0][9]- (update_data['storagecount']*1000)  
        else: 
            print("Number of allocated cores and stroage does not match")    
        values=(response[0][1],response[0][2],response[0][3],response[0][4],response[0][5],response[0][6],response[0][7],update_data['ram'],update_data['cores'],update_data['storage'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was inserted.");  mycursor.close()
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()

@app.route('/editInfra', methods=['POST'])
def editInfra():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        infra_data = json.loads(data)
    
        #{
        #  "id":"2", "Creator id": "Ana mendis",   "Course Name": "Embedded Systems",  "Lab Type": "HDL lab",
        #  "Number of workbench": 20,    "Start Date-Time": "2023-09-28T10:00:32.955Z",  "End Date-Time": "2023-10-28T11:00:32.955Z"
        #}
        mycursor = db.cursor()
        #sql_qry= "UPDATE vlab.vlab.resources SET workbenchId=%s, bookingId=%s, participantId=%s, containerid=%s, directorypath=%s WHERE sno=%s " 
        sql_qry= "UPDATE vlab.resources SET coreallocated=%s, corecredit=%s, corebalance=%s, storageallocated=%s, storagecredit=%s, stroragebalance=%s, ram=%s, totalcores=%s, totalstroage=%s WHERE Sno=%s"
        values=(infra_data['coreallocated'], infra_data['corecredit'], infra_data['corebalance'], infra_data['storageallocated'],infra_data['storagecredit'],infra_data['stroragebalance'], infra_data['ram'], infra_data['totalcores'], infra_data['totalstroage'], infra_data['Sno'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was updated.");   mycursor.close()
        if(mycursor.rowcount>0):    response['Ack']="Infra details updated successfully"
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/listInfra', methods=['POST'])
def listInfra():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        # eg:   {"Lab Type": "HDL lab","Creator id": "Ana mendis"}
        response={"Ack": ""}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        list_infra = json.loads(data)
        mycursor = db.cursor()  
        sql_qry=""; base_qry="SELECT * FROM vlab.resources  " 
        filter_conditions = []
        
        
        try:
            if list_infra['coreallocated']:   filter_conditions.append(f"coreallocated = '{list_infra['coreallocated']}'")
        except: pass
        try:    
            if list_infra['corebalance']:   filter_conditions.append(f"corebalance = '{list_infra['corebalance']}'")
        except: pass
        try:
            if list_infra['totalcores']:   filter_conditions.append(f"totalcores = '{list_infra['totalcores']}'")
        except: pass

        try:
            if list_infra['storageallocated']:   filter_conditions.append(f"storageallocated = '{list_infra['storageallocated']}'")
        except: pass
        try:
            if list_infra['stroragebalance']:   filter_conditions.append(f"stroragebalance = '{list_infra['stroragebalance']}'")
        except: pass
        try:
            if list_infra['totalstroage']:   filter_conditions.append(f"totalstroage = '{list_infra['totalstroage']}'")
        except: pass

        try:    
            if list_infra['ramallocated']:   filter_conditions.append(f"ramallocated = '{list_infra['ramallocated']}'")
        except: pass
        try:    
            if list_infra['rambalance']:   filter_conditions.append(f"rambalance = '{list_infra['rambalance']}'")
        except: pass
        try:    
            if list_infra['totalram']:   filter_conditions.append(f"totalram = '{list_infra['totalram']}'")
        except: pass
        
        if filter_conditions:
            filter_sql = " AND ".join(filter_conditions)
            sql_qry = f"{base_qry} WHERE {filter_sql}"
        else:
            sql_qry = base_qry

        #values=(list_data['Creator id'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall();    mycursor.close()
        print(list);    response['Ack'] = list
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

def checkStroageForImage(imageStroageSize,coreToImageRatio,noOfWorkbench):
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack":""}
        mycursor = db.cursor()
        sql_qry= "SELECT * FROM vlab.resources ORDER BY Sno DESC LIMIT 1  " 
        mycursor.execute(sql_qry)
        storage_info =  mycursor.fetchall();    print(storage_info[0]); mycursor.close()
        totalCores=storage_info[0]['totalcores']; totalStroage=storage_info[0]['totalstroage']  # getting core balance and strorage balance.
        noOfCore= math.ceil(int(noOfWorkbench)/coreToImageRatio)
        storageRequired=imageStroageSize*int(noOfWorkbench)
        print(">>>>",totalCores,totalStroage)
        print(">>>>",noOfCore,storageRequired)
        if(totalCores>noOfCore and totalStroage>storageRequired):
            print("core and stroage available"); response['Ack']="success";
        else:
            print("Resources not available");   response['Ack']="fail";
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    
    return response
# API to update the infrastructure eg: no of cores, ram, memory etc

################## Image details APIs #######################

@app.route('/insertImageDetails', methods=['POST'])
def insertImageDetails():   
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        image_details = json.loads(data)
        mycursor = db.cursor()
        sql_qry= "INSERT INTO vlab.labtype (labName, imageName, description, size,imagePath) VALUES (%s,%s,%s,%s,%s) " 
        values=(image_details['labName'],image_details['imageName'],image_details['description'],image_details['size'],image_details['imagePath'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was inserted.")
        image_id = mycursor.lastrowid
        if(mycursor.rowcount>0):   response['Ack']="Image details inserted successfully with id: "+str(image_id) 
        mycursor.close()
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/editImageDetails', methods=['POST'])
def editImageDetails():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        image_details = json.loads(data)
        mycursor = db.cursor()
        sql_qry= "UPDATE vlab.labtype SET labName=%s, imageName=%s, description=%s, size=%s, imagePath=%s WHERE id=%s " 
        values=(image_details['labName'],image_details['imageName'],image_details['description'],image_details['size'],image_details['imagePath'],image_details['id'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was inserted.")
        image_id = mycursor.lastrowid
        if(mycursor.rowcount>0):   response['Ack']="Image details updated successfully with id: "+str(image_id) 
        mycursor.close()
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/listImageDetails', methods=['POST'])
def listImageDetails():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        # eg:   {"Lab Type": "HDL lab","Creator id": "Ana mendis"}
        response={"Ack": ""}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        list_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry=""; base_qry="SELECT * FROM vlab.labtype  " 
        filter_conditions = []
        try:
            if list_data['labName']:   filter_conditions.append(f"labName = '{list_data['labName']}'")
        except: pass
        try:
            if list_data['imageName']:   filter_conditions.append(f"imageName = '{list_data['imageName']}'")
        except: pass
        try:    
            if list_data['description']:   filter_conditions.append(f"idescription = '{list_data['description']}'")
        except: pass
        try:
            if list_data['size']:   filter_conditions.append(f"size = '{list_data['size']}'")
        except: pass
        try:
            if list_data['imagePath']:   filter_conditions.append(f"imagePath = '{list_data['imagePath']}'")
        except: pass
        

        if filter_conditions:
            filter_sql = " AND ".join(filter_conditions)
            sql_qry = f"{base_qry} WHERE {filter_sql}"
        else:
            sql_qry = base_qry

        #values=(list_data['Creator id'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
        mycursor.execute(sql_qry)
        list =  mycursor.fetchall()
        print(list);    response['Ack'] = list
        mycursor.close()
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

@app.route('/deleteImageDetails', methods=['POST'])
def deleteImageDetails():
    try:
        db = pymysql.connect(host=hostname, user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": ""}
        # {"id":5}
        #try:
        data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
        delete_data = json.loads(data)
        mycursor = db.cursor()
        sql_qry= " DELETE FROM vlab.labtype WHERE id=%s " 
        values=(delete_data['id'],)
        mycursor.execute(sql_qry,values)
        db.commit()
        print(mycursor.rowcount, "was deleted.");   mycursor.close()
        if(mycursor.rowcount>0): response['Ack']="booking was deleted "
        else: response['Ack']="booking Id not found, please check the booking Id. "
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
        
    db.close()
    return response

if __name__ == '__main__':
    create_port_table(6930,6999)
    #app.run(host='0.0.0.0', debug=True)

    # Update the SSL context with the correct file paths
    cert_path = '/etc/ssl/private/fullchain.pem'
    key_path = '/etc/ssl/private/privkey.pem'
    app.run(ssl_context=(cert_path, key_path), host='0.0.0.0', port=5000)


'''
{"Creator id":"Cedric Bauer","Course Name":"VLAB Test room","Lab Type":"demo lab","Number of workbench":2,"Start Date-Time":"2023-10-20T12:00:00.000Z","End Date-Time":"2023-10-21T13:05:00.000Z"}
{"Creator id":"Cedric Bauer","Course Name":"VLAB Test room","Lab Type":"Physics_LAB","Number of workbench":1,"Start Date-Time":"2023-10-22T08:08:00.000Z","End Date-Time":"2023-10-23T10:00:00.000Z"}
'''



