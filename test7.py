start_port = 6900
end_port = 6999
num_ports = 5

free_ports = list(range(start_port, end_port + 1))
in_use_ports=[]
print(free_ports)

def get_free_ports(num_port):
    allocated_ports=[]
    for x in range (0,num_port+1):
        port= free_ports.pop(0)
        allocated_ports.append(port)
    return allocated_ports

def dellocate_port(port):
    in_use_ports.remove(port)
    free_ports.append(port)

available_ports=get_free_ports(5)
for x in available_ports:
    in_use_ports.append(x)
print(in_use_ports)
print(free_ports)
print("----------------------------------------------------------------------")
dellocate_port(6904)
print(in_use_ports)
print(free_ports)

print("/////////////////////////////")
available_ports=get_free_ports(3)
for x in available_ports:
    in_use_ports.append(x)
print(in_use_ports)
print(free_ports)
print("----------------------------------------------------------------------")
dellocate_port(6907)
print(in_use_ports)
print(free_ports)