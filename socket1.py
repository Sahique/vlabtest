import socket

def check_port(host, port):
    try:
        with socket.create_connection((host, port), timeout=1):
            return True
    except (socket.timeout, ConnectionRefusedError):
        return False

# Example usage
host_to_check = "139.14.11.48"  # You can change this to the actual host or IP
ports_to_check = [80, 443, 8080, 6932, 6971]  # List of ports to check

for port in ports_to_check:
    if check_port(host_to_check, port):
        print(f"Port {port} is open")
    else:
        print(f"Port {port} is closed")


def is_port_available(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.bind(("139.14.11.48", port))
            return True
        except OSError:
            return False

# Example usage:
for port1 in ports_to_check:
    port_to_check = port1  # Replace with the port you want to check
    if is_port_available(port_to_check):
        print(f"Port {port_to_check} is available.")
    else:
        print(f"Port {port_to_check} is not available.")


