import subprocess
import docker

import sys
print(sys.executable)


def mount_nfs_volume(USER_ID,container_name, nfs_server, nfs_path, local_mount_path):
    # Get the container ID by name
    response=""
    try:
        container_id = subprocess.check_output(['docker', 'ps', '-q', '-f', f'name={container_name}']).decode('utf-8').strip()
        # Run the mount command inside the container
        # mount -t nfs 139.14.11.68:/mnt/share /home/headless/data -o nolock
        if (USER_ID==""):
            mount_command = f'mount -t nfs -o rw,sync,nolock, {nfs_server}:{nfs_path} {local_mount_path}'
        else:
            mount_command = f'mount -t nfs -o rw,sync,nolock,uid={USER_ID} {nfs_server}:{nfs_path} {local_mount_path}'
        subprocess.run(['docker', 'exec', container_id, 'sh', '-c', mount_command])
        response="success"
    except Exception as e:
        response="exception: ",str(e)
    return response

def umount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path):
    response=""
    try:
        # Get the container ID by name
        container_id = subprocess.check_output(['docker', 'ps', '-q', '-f', f'name={container_name}']).decode('utf-8').strip()
        # Run the mount command inside the container
        mount_command = f'umount -t nfs {local_mount_path} '
        subprocess.run(['docker', 'exec', container_id, 'sh', '-c', mount_command])
        response="success"
    except Exception as e:
        response="exception: ",str(e)
    return response

#val=mount_nfs_volume("2000_8_3", "139.14.11.68", "mnt/shared", "/home/public")
#val=mount_nfs_volume("2000_8_3", "139.14.11.68", "mnt/shared/user_1", "/home/myworkspace")
#val=umount_nfs_volume("2000_8_3", "139.14.11.68", "mnt/shared/user_1", "/home/workspace")
#val=umount_nfs_volume("2000_8_3", "139.14.11.68", "mnt/shared", "/home/public")

#print(val)
'''
if __name__ == "__main__":
    # Docker container name
    container_name = "User_test"

    # NFS server details
    nfs_server = "139.14.11.68"
    nfs_path = "mnt/shared"

    # Local mount path inside the container
    local_mount_path = "/home/headless/data"

    # Mount NFS volume into the Docker container
    mount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path)
'''

# mount -t nfs 139.14.11.68:/mnt/shared {local_mount_path} -o nolock