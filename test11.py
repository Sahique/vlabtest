# import the library
import pymysql

db = pymysql.connect(
    host='localhost',  # This should be the service name defined in Docker Compose
    #host='db',
    user='root',
    password='root',
    db='vlab',  # Replace with your database name
    cursorclass=pymysql.cursors.DictCursor
)
try:
    mycursor = db.cursor()
    print("DB connected")
    mycursor.close()
except:
    print("DB not connected")

# Create tables
with db.cursor() as cursor:
    # Create table for allocated ports
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS allocated_ports (
        id INT AUTO_INCREMENT PRIMARY KEY,
        port INT
    )
    """)

    # Create table for free ports
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS free_ports (
        id INT AUTO_INCREMENT PRIMARY KEY,
        port INT
    )
    """)

# Commit changes and close connection
db.commit()



try:
    with db.cursor() as cursor:
        # Insert port numbers into the free_ports table
        for port in range(6901, 7000):  # 7000 is exclusive
            sql = "INSERT INTO free_ports (port) VALUES (%s)"
            cursor.execute(sql, (port,))
    
    # Commit changes
    db.commit()
    print("Port numbers inserted successfully.")

finally:
    # Close connection
    db.close()




def get_free_ports(num_ports):
    # Connect to MySQL
    connection = pymysql.connect(
        host='localhost',  # This should be the service name defined in Docker Compose
        #host='db',
        user='root',
        password='root',
        db='vlab',  # Replace with your database name
        cursorclass=pymysql.cursors.DictCursor
    )

    try:
        with connection.cursor() as cursor:
            # Fetch num_ports number of free ports from the database
            cursor.execute(f"SELECT port FROM free_ports LIMIT {num_ports}")
            free_ports = [row['port'] for row in cursor.fetchall()]

            if len(free_ports) < num_ports:
                raise ValueError("Insufficient free ports available")

            # Update the status of fetched ports to 'busy'
            for port in free_ports:
                cursor.execute("INSERT INTO allocated_ports (port) VALUES (%s)", (port,))
                cursor.execute("DELETE FROM free_ports WHERE port = %s", (port,))
            
            # Commit changes
            connection.commit()
            return free_ports

    finally:
        pass
        # Close connection
        #connection.close()

# Example usage:
try:
    num_ports_required = 5
    allocated_ports = get_free_ports(num_ports_required)
    print(f"Allocated ports: {allocated_ports}")

    try:
        connection = pymysql.connect(
        host='localhost',  # This should be the service name defined in Docker Compose
        #host='db',
        user='root',
        password='root',
        db='vlab',  # Replace with your database name
        cursorclass=pymysql.cursors.DictCursor
        )
        with connection.cursor() as cursor:
            # Fetch all free ports from the database
            cursor.execute("SELECT port FROM free_ports")
            free_ports = [row['port'] for row in cursor.fetchall()]
            print(free_ports)
            #return free_ports

    finally:
        # Close connection
        connection.close()

except ValueError as e:
    print(f"Error: {str(e)}")


def deallocate_port(port):
    try:
        connection = pymysql.connect(
        host='localhost',  # This should be the service name defined in Docker Compose
        #host='db',
        user='root',
        password='root',
        db='vlab',  # Replace with your database name
        cursorclass=pymysql.cursors.DictCursor
        )
        with connection.cursor() as cursor:
            # Remove the port from the allocated_ports table
            cursor.execute("DELETE FROM allocated_ports WHERE port = %s", (port,))

            # Insert the port into the free_ports table
            cursor.execute("INSERT INTO free_ports (port) VALUES (%s)", (port,))

        # Commit changes
        connection.commit()
        print(f"Port {port} deallocated successfully.")

    except Exception as e:
        # Rollback transaction in case of error
        connection.rollback()
        print(f"Error: {str(e)}")

    finally:
        # Close connection
        connection.close()

# Example usage:
try:
    deallocate_port(6901)  # Example port number to deallocate

except Exception as e:
    print(f"Error: {str(e)}")
