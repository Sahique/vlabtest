
import socket

class PortAllocator:
    def __init__(self, start_port, end_port):
        self.start_port = start_port
        self.end_port = end_port
        self.allocated_ports = []
        self.free_ports = list(range(start_port, end_port + 1))

    def allocate_port(self):
        if not self.free_ports:
            raise ValueError("No free ports available")
        port = self.free_ports.pop(0)
        self.allocated_ports.append(port)
        return port

    def release_port(self, port):
        if port in self.allocated_ports:
            self.allocated_ports.remove(port)
            self.free_ports.append(port)

def find_free_ports(start_port, end_port, num_ports):
    port_allocator = PortAllocator(start_port, end_port)
    free_ports = []

    for _ in range(num_ports):
        free_port = port_allocator.allocate_port()
        free_ports.append(free_port)

    return free_ports


start_port = 6900
end_port = 6999
num_ports = 5

free_ports = find_free_ports(start_port, end_port, num_ports)
print(f"Allocated ports: {free_ports}")
print(PortAllocator.allocated_ports)

free_ports = find_free_ports(start_port, end_port, num_ports)
print(f"Allocated ports: {free_ports}")
#print(PortAllocator.allocate_port)

# Example of releasing a port
#released_port = free_ports[0]
#PortAllocator.release_port(released_port)
#print(f"Released port: {released_port}")
#print(f"Updated free ports: {PortAllocator.free_ports}")


